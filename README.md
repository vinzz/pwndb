# Pwndb.sh

## An other pwndb tool

![pwndb](pwndb.png)

this tool was build for solve limitations of pwndb:

- 2000 result max
- 45 sec of max_execution_time
- only "like results" (for [pwndb.py](https://github.com/davidtavarez/pwndb))



## Usage:

```
-u|--user [USER]          user to check
-U|--user-list [FILE]     file containing users (1 per line)
-e|--exact                check exact user
-d|--domain [DOMAIN]      domain
-D|--domain-list [FILE]   file containing domains (1 per line)
-b|--brute-force [NUMBER] brute force   1 will be A to Z ,
                                        2 will be AA to ZZ
-j|--jobs [number]        number of background jobs (max 10, 5 by default)
-o|--output [file]        output file
-p|--proxy [IP:PORT]      proxy and port of TOR
```

whildecard character is %


#### users


you can use:

- "-u|--user" with the user you want to test
- "-U|--user-list" a file containing all the user you want to test
- "-e|--exact" check for this users exactly

for resultat like "gmail.com" (limit to 2000 results),
or query that will take more than 45 seconde (exemple `-d "%.gouv.fr`)you can use `-b 5`, it will brute force from "aaaaa" to "zzzzz" .. it will be slow but works (specialy for query like "%.domain" (1 day and 20h for "%.gouv.fr"))


#### domains


you can use:

- "-d|--domain" with the domain you want to test
- "-D|--domain-list" a file containing all the user you want to test


#### Background jobs


this will act like false "thread" (background jobs, not real thread).

You can use `-j|--jobs` for set the number of jobs.

You cant set more than 10 background jobs (with more )
 


## exemples:

```
./pwndb.sh -u crime -e -d gmail.com -o result.txt
./pwndb.sh -U user.lst -D domain.lst -p 127.0.0.1:9999
./pwndb.sh -b 2  -d gmail.com -o result.txt
.)pwndb.sh -b 4 -j 10 -d "%.gouv.fr"
```

